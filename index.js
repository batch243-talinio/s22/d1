// console.log("Zawarudo!")

// JS has a built-in functions and methods for arrays. This allows us to manipulate and access array elements.

// [Section] Mutator Methods
	// mutator methods are functions that mutate or change an array after they are created.
	// these methods manipulate the original array performing various tsks as adding and removing elements.

	let fruits = ["apples", "orange", "kiwi", "dragon fruit"];

	console.log(fruits);

	// push() adds element at the end of an array and returns the arrays length.
	/*
		Syntax:
			arrayName.push(elementsToBeAdded);
	*/

	console.log("Current Array Fruit: ");
	console.log(fruits);

	let fruitsLength = fruits.push("Mango");
	console.log("Mutated Array Fruit: ");
	console.log(fruits);
	console.log(fruitsLength);

	// adding multiple elements in an Array
	fruits.push("Avocado", "Guava");
	console.log("Mutated Array Fruit after push: ");
	console.log(fruits);
	console.log(fruitsLength);

	// pop() removes the last element in an array and returns the removed element.
	/*
		Syntax:
			arrayName.push(elementsToBeRemove);
	*/

	console.log("Current Array Fruit: ");
	console.log(fruits);

	let removedArray = fruits.pop();
	console.log("Mutated Array Fruit after pop: ");
	console.log(fruits);
	console.log(removedArray);


	console.log("Current Array Fruit: ");
	console.log(fruits);

	fruits.pop();
	console.log("Mutated Array Fruit after pop: ");
	console.log(fruits);
	console.log(removedArray);

	// unshift() adds 1 or more elements at the beginning of an array and returns length
	/*
		Syntax:
			arrayName.unshift('elementA', ...);
	*/

	console.log("Current Array Fruit: ");
	console.log(fruits);

	fruitsLength = fruits.unshift("Lime", "Banana");
	console.log("Mutated Array Fruit after unshift(): ");
	console.log(fruits);
	console.log(fruitsLength);

	// shift() removes an element at the begining of an array and returns the removed element.
	/*
		Syntax:
			arrayName.shift();
	*/

	console.log("Current Array Fruit: ");
	console.log(fruits);
	removedArray = fruits.shift();

	console.log("Mutated Array Fruit after shift(): ");
	console.log(fruits);
	console.log(removedArray);

	// splice() removes an element from a specified index num and adds element/s.
	/*
		Syntax:
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
	*/

	console.log("Current Array Fruit before splice(): ");
	console.log(fruits);
	removedArray = fruits.splice(1, 1, "Lime");
	console.log(fruits);

	console.log("Mutated Array Fruit after splice(): ");
	console.log(fruits);


	fruits.splice(4, 0, "Cherry");
	console.log(fruits);

	console.log("Current Array Fruit before splice(): ");
	console.log(fruits);

	removedArray = fruits.splice(3, 0, "Durian", "Santol");
	console.log("Mutated Array Fruit after splice(): ");
	console.log(fruits);
	console.log(removedArray);

	// sort() rearranges the array elements in alphanumeric order
	/*
		Syntax:
			arrayName.sort();
	*/

	console.log("Current Array Fruit before sort(): ");
	console.log(fruits);
	fruits.sort(); // returns sorted array.

	console.log("Mutated Array Fruit after sort(): ");
	console.log(fruits);

	/*
		Important!
		 - the sort method is used for more complicated functions.
		 - focus the bootcampers on the basic usage of the sort method.
	*/

	// reverse() reverses the order of array elements
	/*
		Syntax:
			arrayName.reverse();
	*/

	console.log("Current Array Fruit before reverse(): ");
	console.log(fruits);
	console.log(fruits.reverse()); // returns un-sorted array.

	console.log("Mutated Array Fruit after reverse(): ");
	console.log(fruits);


// [Section] Non-Mutated Methods
	/*
		- Non-mutator methods are functions that do not modify or change an array after they are created.
		- these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output.
	*/

	let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

	// indexOf() returns the index number of the first matching element found in an array.
	// - if no match was found, the result will be -1.
	// search process will be done from 1st element proceeding to the last element.
	/*
		Syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, startingIndex);
	*/

	console.log(countries.indexOf('PH')); // finds & returns first array that equals it
	console.log(countries.indexOf('BR')); // returns -1 bcz BR isn't in the array

	// arrayName.indexOf(searchValue, startingIndex);
	console.log(countries.indexOf("PH" , 2))

	// lastIndexOf() returns the index number of the last matching element found in an array.
	// the search process will be done from last element proceeding to the first element.

	/*
		Syntax:
			arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue, startingIndex);
	*/

	console.log(countries.indexOf('PH')); // finds & returns last array that equals it
	
	// slice() gets a portion from an array and returns a new array.
	/*
		Syntax:
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex)
	*/

	let slicedArrayA = countries.slice(2);
	console.log(slicedArrayA);

	let slicedArrayB = countries.slice(1, 5);
	console.log(slicedArrayB);

	// slicing off elements starting from the last elements of an array.

	let slicedArrayC = countries.slice(-3, -1);
	console.log(slicedArrayC);

	// toString() returns an array as a astring separated by commas
	/*
		Syntax:
			arrayName.toString();
	*/

	let stringedArray = countries.toString();
	console.log(stringedArray);

	// concat() combines arrays and returns the combined result
	/*
		Syntax:
			arrayName.concat(arrayB);
			arrayName.concat(elementB);
	*/

	let tasksArrayA = ["drink Html", "eat Javascript"];
	let tasksArrayB = ["inhale CSS", "breath SASS"];
	let tasksArrayC = ["get git", "be node"];
	
	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log("Result from concat method: ");
	console.log(tasks);

	console.log("Result from concat method: ");
	let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);

	console.log(allTasks);

	// Combine arrays with elements

	let combinedTasks = tasksArrayA.concat('Smell Express', 'throw React');
	console.log("Result from concat method: ");
	console.log(combinedTasks);


	// join() returns an array as string seperated by specified separtor string
	/*
		Syntax:
			arrayName.join('stringSeparator');
	*/

	let names = ["john", "Jane", "job", "Jahna"];
	console.log(names.join('-'));

// [Section] Iteration Methods 
	// Iteration Methods are loop designed to perform repetitive tasks on arrays.

	// forEach() is similar to a for loop that iterates on each of array element.
	/*
		Syntax:
			arrayName.forEach(function(indivElement){
				statement/s;
			});
	*/

	console.log(allTasks);
	allTasks.forEach(function(tasks){
		console.log(tasks);
	});


	let filteredTasks = [];

	let task = allTasks.forEach(function(task){
		if(task.length > 10){
			filteredTasks.push(task);
		}
	});
	console.log(task); // undefined it means that forEach does'nt return value
	console.log(filteredTasks);

	// map() iterates on each element and returns new array w/ diff. values depending on the result of the functions operation.
	/*
		Syntax:
			let/const resultArray = arrayName.Map(function(elements){
				statements;
				return;
			})
	*/

	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(number){

		return number * number;
	});

	console.log("Original Array:");
	console.log(numbers);

	console.log("Result of Map Method:");
	console.log(numberMap);

	// every() checks if all elements in an array meet the given condition.
	// useful in validation data stored in arrays specially when dealing w/ large ammounts of data.
	// returns true value when all elements meet the condition otherwise false.
	/*
		Syntax:
			let/const resultArray.every(function(element){
				return expression/condition;
			})
	*/

	console.log(numbers);
	let allValid = numbers.every(function(number){
		return (number < 4);
	});

	console.log(allValid);


	// some() checks if atleast one element in an array meets the given condition.
	/*
		Syntax:
			let/const resultArray.some(function(element){
				return expression/condition;
			})
	*/

	console.log(numbers);
	let someValid = numbers.some(function(number){
		return (number < 2);
	});

	console.log(someValid);

	// filter() returns new array that contains the elements w/c meets the given condition.
	// return an empty array if no element/s were found.
	/*
		Syntax:
			let/const resultArray = arrayName.filter(function(element){
				return expression/condition;
			})
	*/

	console.log(numbers);
	let filterValid = numbers.filter(function(number){
		return (number === 3);

	});
	console.log(filterValid);

	// includes() checks if the argument passed can be found in the array.
	// returns boolean w/c can be saved in variable (T/F = found/notFound).
	// !!KEY SENSITIVE!!
	/*
		Syntax:
			arrayName.includes(argument);
	*/

	let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

	let productFound = products.includes('Mouse');
	console.log(productFound);

	let productFound2 = products.includes('Headset');
	console.log(productFound2);


	// reduce() evaluates elements from left to right and returns/reduces the array into a single value.
	/*
		Syntax:
			let/const resultValue = arrayName.reduce(function(accumulator, currentValue){
				return expression/operation;
			});
	*/

	console.log(numbers);
	let total = numbers.reduce(function(x, y){
		return x + y;
	})

	console.log(total);